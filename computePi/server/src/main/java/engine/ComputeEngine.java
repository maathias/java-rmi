package engine;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import compute.Compute;
import compute.Task;

/**
 * 
 *
 */
public class ComputeEngine implements Compute {

	/**
	 * 
	 */
	public ComputeEngine() {
        super();
    	System.out.println("ComputeEngine constructor");
    }

	/**
	 * 
	 */
    public <T> T executeTask(Task<T> t) {
    	System.out.println("execute the given task");
        return t.execute();
    }

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
		
		String hostname = "localhost";
    	
        System.out.println("Starting application"); 
        
		System.setProperty("java.rmi.server.hostname", hostname);
		System.setProperty("java.rmi.server.codebase", "http://" + hostname + "/classes/ComputePi/");
		System.setProperty("java.security.policy", "http://" + hostname + "/classes/ComputePi/server.policy");

        System.out.println("Installing security manager");
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

        try {
            String name = "Compute";
            Compute engine = new ComputeEngine();
            System.out.println("Creating stub");
            Compute stub =
                (Compute) UnicastRemoteObject.exportObject(engine, 0);
            System.out.println("Locate registry");
            Registry registry = LocateRegistry.getRegistry();
            System.out.println("Bind stub to registry");
            registry.rebind(name, stub);
            System.out.println("Server ready, waiting for client requests");
        } catch (Exception e) {
            System.err.println("ComputeEngine exception:");
            e.printStackTrace();
        }
    }
}
